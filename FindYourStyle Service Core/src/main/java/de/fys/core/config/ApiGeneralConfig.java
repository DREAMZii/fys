package de.fys.core.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.filter.HttpPutFormContentFilter;

import javax.servlet.Filter;
import javax.sql.DataSource;

@Configuration
public class ApiGeneralConfig {
    /**
     * Bean for retrieving datasource.
     * @return Initialized DataSource.
     */
    @Bean
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource apiDataSource(){
        return DataSourceBuilder.create().build();
    }

    /**
     * Bean for retrieving JdbcTemplate.
     * @param dataSource Instance of DataSource.
     * @return Instance of JdbcTemplate.
     */
    @Bean
    public JdbcTemplate apiJdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    /**
     * Bean to enable RequestBody for PUT-Requests.
     * @return Instance of HttpPutFormContentFilter.
     */
    @Bean
    public Filter httpPutFormContentFilter() {
        return new HttpPutFormContentFilter();
    }
}
