package de.fys.core;

import org.apache.log4j.Logger;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@ComponentScan({"de.fys.core", "de.fys.da"})
@EntityScan({"de.fys.da"})
@EnableJpaRepositories({"de.fys.da"})
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
@EnableScheduling
public class ApiLauncher {
    private static Logger logger = Logger.getLogger(ApiLauncher.class);

    /**
     * Main entry method for rest api.
     * @param args Optional parameters passed via console.
     */
    public static void main(String[] args){
        SpringApplication app = new SpringApplication(ApiLauncher.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        logger.info("Started Spring Application!");
    }
}
