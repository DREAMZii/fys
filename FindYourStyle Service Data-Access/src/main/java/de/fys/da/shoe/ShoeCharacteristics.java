package de.fys.da.shoe;

import java.io.Serializable;

public enum ShoeCharacteristics implements Serializable {
    ;

    private final String name;

    ShoeCharacteristics(String name) {
        this.name = name;
    }
}
