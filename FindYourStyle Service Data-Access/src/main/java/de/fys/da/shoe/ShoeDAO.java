package de.fys.da.shoe;

import java.util.List;

public interface ShoeDAO {
    List<Shoe> findAll();

    Shoe findShoeById(long id);

    void delete(long id);

    Shoe create(ShoeDTO dto);
}
