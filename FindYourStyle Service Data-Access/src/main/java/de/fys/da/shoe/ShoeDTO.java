package de.fys.da.shoe;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ShoeDTO {
    private String name;
    private String brand;
    private String shoeType;
    private List<String> characteristics;
    private String pictureName;

    public static class Builder {
        private String name;
        private String brand;
        private String shoeType;
        private List<String> characteristics;
        private String pictureName;

        public Builder(String name, String brand, String shoeType) {
            this.name = name;
            this.brand = brand;
            this.shoeType = shoeType;
        }

        public Builder withPictureName(String pictureName) {
            this.pictureName = pictureName;
            return this;
        }

        public Builder withCharacteristics(String... characteristics) {
            this.characteristics = Arrays.asList(characteristics);
            return this;
        }

        public ShoeDTO build() {
            return new ShoeDTO(name, brand, shoeType, characteristics, pictureName);
        }
    }
}
