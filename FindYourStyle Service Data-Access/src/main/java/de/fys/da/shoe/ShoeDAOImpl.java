package de.fys.da.shoe;

import de.fys.da.DAOManager;
import de.fys.da.dao.Injectable;
import de.fys.da.dao.BaseDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShoeDAOImpl extends BaseDAO implements ShoeDAO, Injectable {
    private static Logger logger = Logger.getLogger(ShoeDAOImpl.class);

    @Autowired
    private ShoeRepository shoeRepository;

    @Override
    public void inject(DAOManager daoManager) {
        BufferedImage image;
        List<Color> colors = new ArrayList<>();
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/Shoe.jpg"));
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    int color = image.getRGB(x, y);

                    int  red = (color & 0x00ff0000) >> 16;
                    int  green = (color & 0x0000ff00) >> 8;
                    int  blue = color & 0x000000ff;

                    Color ciao = new Color(red, green, blue);
                    if (!colors.contains(ciao)) {
                        colors.add(ciao);
                    }
                }
            }

            colors.forEach(logger::info);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Shoe> findAll() {
        return shoeRepository.findAll();
    }

    @Override
    public Shoe findShoeById(long id) {
        return shoeRepository.findOne(id);
    }

    @Override
    public void delete(long id) {
        shoeRepository.delete(id);
    }

    @Override
    public Shoe create(ShoeDTO dto) {
        return shoeRepository.save(new Shoe(dto.getName(), dto.getBrand(), dto.getShoeType(), dto.getCharacteristics(), dto.getPictureName()));
    }
}
