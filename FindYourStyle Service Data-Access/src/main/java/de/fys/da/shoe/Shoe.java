package de.fys.da.shoe;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table
@Getter
public class Shoe implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String shoeType;

    @ElementCollection
    private List<String> characteristics = new ArrayList<>();

    @Column
    private String pictureName;

    protected Shoe() {
        // Required
    }

    public Shoe(String name, String brand, String shoeType) {
        this.name = name;
        this.brand = brand;
        this.shoeType = shoeType;
    }

    public Shoe(String name, String brand, String shoeType, List<String> characteristics, String pictureName) {
        this.name = name;
        this.brand = brand;
        this.shoeType = shoeType;
        this.characteristics = characteristics;
        this.pictureName = pictureName;
    }
}
