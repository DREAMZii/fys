package de.fys.da.dao;

import de.fys.da.DAOManager;

public interface Injectable {
    void inject(DAOManager daoManager);
}
